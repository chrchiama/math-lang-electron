import React, { Component } from 'react';
import logo from './logo.svg';
import './style/App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>React + Electron = <span role="img" aria-label="love">😍</span></h2>
        </div>
      </div>
    );
  }
}

export default App;
